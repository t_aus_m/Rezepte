# Veganer Käsekuchen

Von: Rewe
Link: https://www.rewe.de/rezepte/veganer-kaesekuchen/
Portionen: 12
Bild: https://i.rewe-static.de/rezepte/rewe-de/rezepte-und-gesund-geniessen/rezepte/vegane-rezepte/vegane-kuchen/veganer_kaesekuchen/veganer_kaesekuchen_rdk-rds_rv_hd.png
Dauer: 30 min

## Zutaten

### Für den Teig

Mehl: 250 g
Backpulver: 2 TL
Margarine: 125 g
Zucker: 80 g
Salz: 1 Prise
kaltes Wasser: 3 EL

### Für den Sojajoghurt

Sojajoghurt: 1 kg
Zucker: 150 g
Zitronenschale: 1 Päckchen
Margarine: 125 g
Vanillepuddingpulver: 2 Päckchen

## Zubereitung

1. Für den Boden Mehl, Backpulver, Margarine, Zucker, Salz und Wasser zu einem glatten Teig verkneten. Ca. 30 Minuten kühl stellen.
2. Sojajoghurt mit Zucker, Zitronenschale und Puddingpulver verrühren. Margarine schmelzen und unterrühren.
3. Eine Springform (26 cm) fetten und den Teig darin gleichmäßig verteilen. Hierbei einen Rand von 2-3 cm Höhe bilden.
4. Die Joghurtmasse in die Form füllen und den Kuchen im vorgeheizten Backofen (180 °C Ober-/Unterhitze) ca. 60 Minuten goldgelb backen. Falls der Kuchen zu dunkel wird mit Alufolie abdecken.