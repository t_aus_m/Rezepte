# Paprika-Bohnen-Aufstrich

Von: kleenesschokimonster
Link: https://www.chefkoch.de/rezepte/1581991265653535/Chili-Bohnen-Aufstrich.html
Portionen: 1
Dauer: 20 min
Bild: https://img.chefkoch-cdn.de/rezepte/1581991265653535/bilder/697744/crop-360x240/chili-bohnen-aufstrich.jpg

## Zutaten

Zwiebeln: 2 mittelgroße
Knoblauchzehen: 1 bis 2
Olivenöl: 20 ml
getrocknete Kidneybohnen: 125 g
Tomatenmark oder Passata: 100 ml
Basilikum, Oregano, Paprikapülver (edelsüß), Chilipulver, Currypulver: nach Belieben

## Zubereitung

1. Kidneybohnen einweichen lassen und weichkochen
2. Zwiebeln hacken und mit dem Olivenöl in eine Pfanne geben
3. Knoblauch dazu pressen und beides anbraten
4. Gewürze dazu geben, kurz andünsten
5. Kidneybohnen dazu geben, kurz anbraten
6. Tomatenmark/Passata dazugeben, kurz aufkochen und bei bedarf reduzieren lassen
7. Alles pürieren
