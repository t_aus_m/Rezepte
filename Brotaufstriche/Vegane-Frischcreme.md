# Vegane Frischcreme

Von: Ina
Portionen: 1
Dauer: 10 min
Bild: /static/images/frischcreme.jpg


## Zutaten

Cashewkerne: 50 g
Wasser: 90 ml
Naturtofu: 200 g
Salz: 5 g
Zitronensaft: 10 ml
Olivenöl: 10 ml
Kräuter: nach Belieben

## Zubereitung

1. Alle Zutaten in ein Gefäß geben und mit einem Pürierstab pürieren und vermischen. Den Tofu ggf. vorher etwas kleiner schneiden.
