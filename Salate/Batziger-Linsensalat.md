# Batziger Linsensalat

Von: Vero
Portionen: 1
Bild: /static/images/batziger-linsensalat.jpg
Dauer: 30 min

## Zutaten

Sojageschnetzeltes: 125 g
Linsen: 200 g
Zwiebeln: eine, ca. 150 g
Sojadrink: 100 ml
Knoblauch: 1 Zehe
Sojasauße: ca. 30 ml
Zitronensaft oder Essig: ca. 15 ml
Balsamico (dunkel): ca. 20 ml
Majoran
Paprikapulver
Pfeffer
Öl (Raps oder Sonnenblumen): ca. 200 ml

## Zubereitung

1. Sojageschnetzeltes mit kochendem Wasser (ca. ??? ml) aufgießen und ziehen lassen
2. Linsen in einem Topf bissfest kochen
3. Zwiebeln würfeln und in einer Pfanne glasig braten
4. Sojageschnetzeltes abgießen, Restfeuchtigkeit ausdrücken und mit in die Pfanne geben
5. Knoblauch fein hacken/pressen und in die Pfanne geben
6. Sojasoße in die Pfanne geben
7. Inhalt der Pfanne mit Majoran, Paprikapulver, Pfeffer und Balsamico abschmecken. Salz sollte nicht notwendig sein, da genug Sojasauce hinzugegeben wurde
8. Für die Sojanese 100 ml Sojadrink und ca. 15 ml Zitronensaft/Essig in ein hohes Gefäß geben
9. Unter ständigem mixen mit dem Pürrierstab Öl hinzugeben, bis die Masse steif wird
10. Linsen abgießen und mit dem Pfanneninhalt und der Sojanese verrühren, ggf. nochmals abschmecken und nachwürzen
