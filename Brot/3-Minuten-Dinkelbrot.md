# 3-Minuten-Dinkelbrot

Von: Mama
Portionen: 1
Dauer: 75 min

## Zutaten
Dinkelmehl: 500 g
Hefe: 1 Würfel
lauwarmes Wasser: 500 ml
Leinsamen: 50 g
Sesam: 50 g
Sonnenblumenkerne: 50 g
Salz: 2 TL
Obstessig: 2 EL

## Zubereitung

1. Wasser mit Hefe verrühren
2. Alle weiteren Zutaten hinzufügen
3. Mit Knethaken kurz durchmischen
4. Den Teig in eine gefettete Kastenform geben
5. Den Teig in den kalten Ofen stellen, nicht gehen lassen
6. Bei 200 °C auf mittlerer Schiene für 60 min backen
