# Gnocchi mit Tomaten und Mozzarella

Von: iglantine
Link: https://www.chefkoch.de/rezepte/1911411311572123/Gnocchi-mit-Tomaten-und-Mozzarella.html
Portionen: 2
Bild: https://img.chefkoch-cdn.de/rezepte/1911411311572123/bilder/1025092/crop-600x400/gnocchi-mit-tomaten-und-mozzarella.jpg 600w"
Dauer: 15 min

## Zutaten

Gnocchi: 400 g
Mozarrella: 1 Kugel
Cocktailtomaten: 150 g
Basilikum: 1 Hand voll
Knoblauch: 1 Zehe
Salz
Pfeffer
Olivenöl

## Zubereitung

1. Gnocchi nach Packungsanleitung zubereiten
2. Mozarrella würfeln, Cocktaqiltomaten halbieren, Knoblauch hacken
3. Knoblauch und halbierte Cocktailtomaten in heißem Olivenöl so lange anschwitzen, bis die Tomaten anfangen zusammenzufallen. Gnocchi, gewürfelten Mozzarella und gehacktes Basilikum dazugeben. Bei mittlerer Hitze unter ständigem Rühren den Mozzarella schmelzen lassen. Mit Salz und Pfeffer abschmecken.
