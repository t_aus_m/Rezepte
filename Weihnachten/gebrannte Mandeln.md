# gebrannte Mandeln

Von: Mamas Kuche
Link: https://mamaskuche.com/superleckere-gebrannte-mandeln/
Bild: https://mamaskuche.com/wp-content/uploads/2020/10/superleckere-gebrannte-mandeln.jpg
Dauer: 45 min

## Zutaten
Mandeln: 200 g
Zucker: 200 g
Wasser: 100 ml
Vanillezucker: 14 g (?)
Zimt: 1 EL

## Zubereitung
1. Das Wasser mit Zucker, Zimt und Vanillezucker in einer beschichteten Pfanne zum Kochen bringen.
2. Wenn die Masse kocht, die Mandeln zugeben und weiterhin auf hoher Temperatur kochen lassen.
3. Solange kochen lassen, bis der Zucker getrocknet ist – dabei ständig umrühren.
4. Den Herd auf mittlere Stufe herunterschalten und weiterrühren, bis der Zucker wieder anfängt zu schmelzen und die Mandeln karamellisieren.
5. Die gebrannten Mandeln auf ein Blech (am besten mit Backpapier auslegen) kippen und sofort voneinander trennen, damit sie nicht zusammen kleben.
