# Vegane Zimtsterne

Von: Peta 
Link: https://www.peta.de/rezepte/zimtsterne/
Portionen: 60
Bild: https://www.peta.de/wp-content/uploads/2020/11/2017-12-05-Zimtsterne-Teller-990x656.jpg
Dauer: 20 min

## Zutaten 

### Für den Teig
Puderzucker: 200 g
Zimt: 2 EL
Wasser: 8 EL
Zitronensaft: 1 EL
gemahlene Haselnüsse: 200 g
gemahlene Mandeln: 200 g
geriebene Orangenschale: 1 EL

### Für die Glasur
Puderzucker: 150 g
Wasser: etwas
Zimt: 1 TL

## Zubereitung

1. Alle Zutaten für den Teig, sorgfältig miteinander vermengen. Nicht wundern, der Teig ist sehr klebrig.
2. Die Arbeitsfläche großzügig mit Mehl bestäuben und den Teig ca. 1 cm dick ausrollen. Falls der Teig sich nur sehr schwer verarbeiten lässt, während der Verarbeitung nach und nach etwas Mehl unterkneten.
3. Den Backofen auf 230°C vorheizen.
4. Mit einem Plätzchenausstecher Sterne ausstechen und auf ein mit Backpapier bestücktes Blech legen. Teigreste wieder zügig zusammenfügen, ausrollen und ebenfalls ausstechen, bis der ganze Teig verbraucht ist.
5. Die Kekse im Ofen für 5-7 Minuten backen, nicht länger. Die Sterne sind noch relativ weich, wenn sie aus dem Ofen kommen, werden beim Abkühlen aber noch fest. Die Kekse auf einem Kuchengitter abkühlen lassen.
6. In der Zwischenzeit für die Glasur Puderzucker und Zimt mischen und mit gerade so viel Wasser verrühren, bis eine dickflüssige Masse entsteht. Falls die Glasur zu dünn wird, etwas mehr Puderzucker hinzugeben.
7. Die abgekühlten Zimtsterne mit der Glasur bestreichen.
